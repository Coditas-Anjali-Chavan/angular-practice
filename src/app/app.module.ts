import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { SmsAppComponent } from './components/sms-app/sms-app.component';
import {FormsModule} from "@angular/forms";
// import { ChangeUsernameManualComponent } from './components/change-username-manual/change-username-manual.component';
// import { ChangeUsernameAutoComponent } from './components/change-username-auto/change-username-auto.component';
// import { ShowPasswordComponent } from './components/show-password/show-password.component';
// import { ShowRangeComponent } from './components/show-range/show-range.component';
// import { ThemeChangeComponent } from './components/theme-change/theme-change.component';
// import { UserRegisterFormComponent } from './components/user-register-form/user-register-form.component';
// import { AuthUserComponent } from './components/auth-user/auth-user.component';
import { PipesComponent } from './components/pipes/pipes.component';
// import { CardComponent } from './components/card/card.component';

// import { CounterComponent } from './components/counter/counter.component';
// import { ProductItemComponent } from './components/product-item/product-item.component';
// import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
// import { WishMsgComponent } from './components/wish-msg/wish-msg.component';



@NgModule({
  declarations: [
    AppComponent,
    PipesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
