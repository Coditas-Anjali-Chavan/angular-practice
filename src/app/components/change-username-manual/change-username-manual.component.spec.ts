import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeUsernameManualComponent } from './change-username-manual.component';

describe('ChangeUsernameManualComponent', () => {
  let component: ChangeUsernameManualComponent;
  let fixture: ComponentFixture<ChangeUsernameManualComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeUsernameManualComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangeUsernameManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
