import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-change-username-manual',
  templateUrl: './change-username-manual.component.html',
  styleUrls: ['./change-username-manual.component.css']
})
export class ChangeUsernameManualComponent implements OnInit {

  public username: string = '';

  constructor() {}

  ngOnInit(): void {}

  updateInput(event: any): void {
    this.username = event.target.value;
  }

}
