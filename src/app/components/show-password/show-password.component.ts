import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-password',
  templateUrl: './show-password.component.html',
  styleUrls: ['./show-password.component.css']
})
export class ShowPasswordComponent implements OnInit {
   public inputType:string='password';
   public showPassword(event:Event):void{
    //using ternary opearator
    // event.target.checked ? this.inputType='text' : this.inputType='password';
    if ((<HTMLInputElement>event.target).checked) {
      this.inputType = 'text';
    } else {
      this.inputType = 'password';
    }

   }
  constructor() { }

  ngOnInit(): void {
  }

}
