import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-theme-change',
  templateUrl: './theme-change.component.html',
  styleUrls: ['./theme-change.component.css']
})
export class ThemeChangeComponent implements OnInit {
  public darkTheme:boolean=false;
  constructor() { }

  ngOnInit(): void {
  }

}
