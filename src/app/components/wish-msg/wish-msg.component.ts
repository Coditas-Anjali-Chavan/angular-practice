import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wish-msg',
  templateUrl: './wish-msg.component.html',
  styleUrls: ['./wish-msg.component.css']
})
export class WishMsgComponent implements OnInit {
  public message: String = 'Hello';
  constructor() {}

  ngOnInit(): void {}

  public updateMessage(msg: string): void {
    this.message = msg;
  }


}
