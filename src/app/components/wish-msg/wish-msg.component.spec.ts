import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WishMsgComponent } from './wish-msg.component';

describe('WishMsgComponent', () => {
  let component: WishMsgComponent;
  let fixture: ComponentFixture<WishMsgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WishMsgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WishMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
