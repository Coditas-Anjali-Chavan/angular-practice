import { NumberSymbol } from '@angular/common';
import { Component, OnInit } from '@angular/core';

interface Employee
{
  sno:string,
  name:string,
  age:number,
  designation:string,
  doj:Date,
  salary:Number
}
@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {
  public employee:Employee=
  {
    sno:"ab121",
    name:"anjali",
    age:21,
    designation:"engineer",
    doj:new Date(),
    salary:450000

  };
  constructor() { }

  ngOnInit(): void {
  }

}
