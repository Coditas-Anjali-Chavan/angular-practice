import { Component, OnInit } from '@angular/core';

interface User{
  name:string;
  email:string,
  password:string,
  designation:string,
  bio:string,
  terms:boolean
}
 
@Component({
  selector: 'app-user-register-form',
  templateUrl: './user-register-form.component.html',
  styleUrls: ['./user-register-form.component.css']
})
export class UserRegisterFormComponent implements OnInit {
  public user:User={
    name:'',
    email:'',
    password:'',
    designation:'',
    bio:'',
    terms:false
  }
  constructor() { }

  ngOnInit(): void {
  }
  public submitRegister():void
  {
   console.log(this.user);
  }
}
